package kz.adilov.demomvvm.service.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import kz.adilov.demomvvm.network.ApiImpl;
import kz.adilov.demomvvm.service.model.News;
import kz.adilov.demomvvm.utility.Logger;

public class NewsRepository {
    private static NewsRepository newsRepository;
    private CompositeDisposable disposables = new CompositeDisposable();

    private NewsRepository() {
    }

    public synchronized static NewsRepository getInstance() {
        if (newsRepository == null) {
            newsRepository = new NewsRepository();
        }
        return newsRepository;
    }

    public LiveData<News> getNews(String category) {
        final MutableLiveData<News> data = new MutableLiveData<>();
        disposables.add(ApiImpl.getInstance()
                        .getNews(category)
//                .doOnSubscribe(action -> showProgressTemplateView())
                        .subscribeWith(new DisposableObserver<News>() {
                            @Override
                            public void onNext(News news) {
                                Logger.e(getClass(), "onNext");
                                data.setValue(news);
                            }

                            @Override
                            public void onError(Throwable e) {
                                Logger.e(getClass(), "onError");
                                data.setValue(null);
//                        handleError(e);
//                        showFailTemplateView();

                            }

                            @Override
                            public void onComplete() {
                                Logger.e(getClass(), "onCompleted");
//                        showSuccessTemplateView();
                            }
                        })
        );

        return data;
    }

    private void dispose() {
        if (disposables != null && !disposables.isDisposed()) {
            disposables.dispose();
        }
    }

}
