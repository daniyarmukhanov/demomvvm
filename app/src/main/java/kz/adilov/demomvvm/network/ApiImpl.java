package kz.adilov.demomvvm.network;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import kz.adilov.demomvvm.service.model.News;

/**
 * Created by Yes Adilov on 26.09.17.
 */

public class ApiImpl implements Api {

    private static ApiImpl apiImpl;
    private Api api;

    public static ApiImpl getInstance() {
        if (apiImpl == null) {
            RetrofitServiceGenerator retrofitServiceGenerator = RetrofitServiceGenerator.getInstance();
            apiImpl = new ApiImpl(retrofitServiceGenerator.createService(Api.class));
        }
        return apiImpl;
    }

    private ApiImpl(Api api) {
        this.api = api;
    }

    @Override
    public Observable<News> getNews(String category) {
        return api
                .getNews(category)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}