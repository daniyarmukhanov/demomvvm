package kz.adilov.demomvvm.ui.detail;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;

import kz.adilov.demomvvm.R;
import kz.adilov.demomvvm.databinding.ActivityNewsDetailBinding;


public class NewsDetailActivity extends FragmentActivity {

    private ActivityNewsDetailBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_news_detail);
        binding.setUrl(getIntent().getStringExtra("url"));
    }

}
