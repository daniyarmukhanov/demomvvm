package kz.adilov.demomvvm.ui.main;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kz.adilov.demomvvm.R;
import kz.adilov.demomvvm.adapter.NewsAdapter;
import kz.adilov.demomvvm.databinding.MainFragmentBinding;

public class MainFragment extends Fragment {

    private MainViewModel mViewModel;

    private NewsAdapter newsAdapter;
    private MainFragmentBinding binding;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.main_fragment, container, false);

        newsAdapter = new NewsAdapter();
        binding.projectList.setAdapter(newsAdapter);
        binding.setIsLoading(true);

        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        observeViewModel();
    }

    private void observeViewModel() {
        // Update the list when the data changes
        mViewModel.getObservableNews().observe(this, news -> {
            if (news != null) {
                binding.setIsLoading(false);
                newsAdapter.setProjectList(news.getArticles());
            }
        });
    }
}
